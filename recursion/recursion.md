# recursion

## 全排列 (lc 46)
思路：递归。每次递归时遍历输入数组，查找一个还未加入排列的数，使用哈希表在O(1)时间内判断一个数是否已经放入排列。
拓展：
1. 尽可能减少递归栈，参数栈。结果数组、哈希表都使用全局变量。
代码：
```
#define MAX_NUM (10 - (-10) + 1)
#define OFFSET 10
vector<vector<int>> result;
vector<int> visited;
class Solution {
public:
    void permutation(vector<int>& nums, vector<int> &seq)
    {
        if (seq.size() == nums.size()) {
            result.push_back(seq);
            return;
        }
        for (int num : nums) {
            if (!visited[num + OFFSET]) {
                visited[num + OFFSET] = 1;
                seq.emplace_back(num);
                permutation(nums, seq);
                seq.pop_back();
                visited[num + OFFSET] = 0;
            }
        }
    }
    vector<vector<int>> permute(vector<int>& nums) {
        vector<int> seq;

        result = vector<vector<int>>();
        visited = vector<int>(MAX_NUM, 0);

        permutation(nums, seq);
        return result;
    }
};
```