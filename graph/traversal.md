# traversal

## 岛屿数量 (lc 200)
思路：将矩阵看作m*n的点阵，每个'1'点与上下左右的'1'点间存在一条边，'0'点视作不存在的点。问题转化为求连通组件个数。标准解法为运行Full DFS，每个DFS运行完岛屿数++。
复杂度：
1. DFS：时间 O(|E|) 空间 O(|V|^2)
2. BFS：时间 O(|V|+|E|) 空间 O(|V|^2)
代码：
```
class Solution {
public:
    void visit(int point, vector<vector<int>> &adj, unordered_set<int> &not_visited)
    {
        not_visited.erase(point);
        for (int adjp : adj[point]) {
            if (not_visited.find(adjp) != not_visited.end()) {
                visit(adjp, adj, not_visited);
            }
        }
    }

    int numIslands(vector<vector<char>>& grid) {
        int m = grid.size(), n = grid[0].size(), island = 0;
        vector<vector<int>> adj(m * n);
        unordered_set<int> not_visited;
        queue<int> q;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    int base = i * n + j;
                    vector<int> &cur = adj[base];
                    if (i && grid[i - 1][j] == '1') {
                        cur.emplace_back(base - n);
                    }
                    if (i < m - 1 && grid[i + 1][j] == '1') {
                        cur.emplace_back(base + n);
                    }
                    if (j && grid[i][j - 1] == '1') {
                        cur.emplace_back(base - 1);
                    }
                    if (j < n - 1 && grid[i][j + 1] == '1') {
                        cur.emplace_back(base + 1);
                    }
                    not_visited.insert(base);
                }
            }
        }

        while (!not_visited.empty()) {
/* BFS
            q.push(*not_visited.begin());
            while (!q.empty()) {
                int point = q.front();
                q.pop();
                if (not_visited.find(point) != not_visited.end()) {
                    for (int adjp : adj[point]) {
                        if (not_visited.find(adjp) == not_visited.end()) {
                            continue;
                        }
                        q.push(adjp);
                    }
                    not_visited.erase(point);
                }
            }
*/
/* DFS */
            int s = *not_visited.begin();

            visit(s, adj, not_visited);

            island++;
        }
        return island;
    }
};
```

## 腐烂的橘子 (lc 994)
思路：将矩阵看作m*n的点阵，每个'1'或'2'点与上下左右的'1'或'2'点间存在一条边，'0'点视作不存在的点。问题转化为求所有'2'点到其他'1'点最短路径的最大值。标准解法为BFS。
复杂度：时间 O(|V|+|E|) 空间 O(|V|^2)
拓展：
1. 邻接表可以用map或unordered_map，但pair类型不支持哈希特性，使用unordered_map需要自己构造hash函数。
代码：
```
#include <utility>

struct pairhash {
    template <typename T, typename U>
    size_t operator()(const pair<T,U> &p) const
    {
        return hash<T>()(p.first) ^ hash<U>()(p.second);
    }
};

class Solution {
public:
    int orangesRotting(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        unordered_map<pair<int,int>, vector<pair<int,int>>, pairhash> adj;
        queue<pair<int,int>> q;
        vector<vector<int>> not_visited(m, vector<int>(n, 0));
        int tick = 0, fresh = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 2 || grid[i][j] == 1) {
                    pair<int,int> point = make_pair(i, j);
                    auto &adjv = adj[point];
                    if (i && grid[i - 1][j]) {
                        adjv.emplace_back(i-1,j);
                    }
                    if (i < m - 1 && grid[i + 1][j]) {
                        adjv.emplace_back(i+1,j);
                    }
                    if (j && grid[i][j - 1]) {
                        adjv.emplace_back(i,j-1);
                    }
                    if (j < n - 1 && grid[i][j + 1]) {
                        adjv.emplace_back(i,j+1);
                    }

                    if (grid[i][j] == 2) {
                        q.push(point);
                    } else {
                        fresh++;
                    }
                    not_visited[i][j] = 1;
                }
            }
        }

        while (!q.empty() && fresh) {
            int num = q.size();

            for (int k = 0; k < num; k++) {
                auto point = q.front();
                q.pop();
                if (not_visited[point.first][point.second]) {
                    for (auto adjp : adj[point]) {
                        if (grid[adjp.first][adjp.second] == 1) {
                            q.push(adjp);
                            grid[adjp.first][adjp.second] = 2;
                            fresh--;
                        }
                    }
                    not_visited[point.first][point.second] = 1;
                }
            }
            tick++;
        }

        return fresh ? -1 : tick;
    }
};
```

## 课程表 (lc 207)
思路：每组先修课程都是一条有向路径，整个先修课程数组构成一个有向图。寻找是否存在环路。标准解法为采用FULL DFS遍历全图，对访问过的点进行标记剪枝。
复杂度：时间 O(|E|)，空间 O(|V|+|E|)
拓展：
1. 查找未访问的点可以用哈希表实现查找、增删O(1)。目前的实现需要O(|V|)
代码：
```
class Solution {
public:
    int find_next_course(int cur, vector<int> &visited)
    {
        int end, round = 0;

        if (cur >= visited.size()) {
            cur = 0;
        }

        end = cur;
        while (visited[cur]) {
            if (round && cur == end) {
                cur = -1;
                break;
            }
            cur++;
            if (cur >= visited.size()) {
                cur = 0;
                round = 1;
            }
        }
        return cur;
    }

    int visit(int course, vector<vector<int>> &adj, vector<int> &visited, vector<int> &prev)
    {
        int ret = 0;
 
        if (prev[course]) {
            return -1;
        }

        prev[course] = 1;

        for (int adjc : adj[course]) {
            if (visited[adjc]) {
                continue;
            }
            if (visit(adjc, adj, visited, prev) < 0) {
                ret = -1;
                break;
            }
        }

        prev[course] = 0;
        visited[course] = 1;

        return ret;
    }

    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<int>> adj(numCourses);
        vector<int> visited(numCourses, 0), prev(numCourses, 0);
        int course = 0;
        bool ret = true;

        for (vector<int> &straint : prerequisites) {
            adj[straint[1]].emplace_back(straint[0]);
        }

        for (int i = 0; i < numCourses; i++) {
            if (!adj[i].size()) {
                course = i;
                break;
            }
        }

        while ((course = find_next_course(course, visited)) >= 0) {

            if (visit(course, adj, visited, prev) < 0) {
                ret = false;
                break;
            }

        }

        return ret;
    }
};
```

## 课程表II (lc 210)
思路：对整个课程有向图进行拓扑排序。标准解法为FULL DFS遍历整个有向图，每个节点的完成顺序的倒序即为拓扑排序。
复杂度：时间 O(n)，空间 O(|V|+|E|)
代码：
```
#include <algorithm>
class Solution {
    vector<vector<int>> adj;
    vector<int> visited;
    vector<int> prev;

public:
    int find_next_course(int course, int numCourses)
    {
        int cur = course;

        while (visited[cur]) {
            cur++;
            if (cur >= numCourses) {
                cur = 0;
            }

            if (cur == course) {
                cur = -1;
                break;
            }
        }

        return cur;
    }

    int visit(int course, vector<int> &result)
    {
        if (prev[course]) {
            return -1;
        }

        prev[course] = 1;

        for (int adjc : adj[course]) {
            if (visited[adjc]) {
                continue;
            }

            if (visit(adjc, result) < 0) {
                return -1;
            }
        }

        prev[course] = 0;
        visited[course] = 1;
        result.emplace_back(course);

        return 0;
    }

    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
        vector<int> result;
        int s, course;
        int min_in_degree = numCourses;

        adj = vector<vector<int>> (numCourses);
        visited = vector<int> (numCourses, 0);
        prev = vector<int>(numCourses, 0);

        for (vector<int> &straint : prerequisites) {
            adj[straint[1]].emplace_back(straint[0]);
            visited[straint[0]]++;
        }

        for (int i = 0; i < numCourses; i++) {
            if (!visited[i]) {
                s = i;
                break;
            } else if (visited[i] < min_in_degree) {
                min_in_degree = visited[i];
                s = i;
            }
        }

        memset(&visited[0], 0, numCourses * sizeof(int));

        course = s;
        while ((course = find_next_course(course, numCourses)) >= 0) {
            if (visit(course, result) < 0) {
                result = vector<int>();
                break;
            }
        }

        reverse(result.begin(), result.end());

        return result;
    }
};
```

## 不同路径III (lc 980)
思路：当一条路径访问了所有的点并且走到终点时，就找到了一条路径。对全图做dfs，并在访问完所有"0"点时达到终点，即增加一条路径。
拓展：
1. 在已经访问了点集S中所有的点的情况下，访问下一个点(i,j)的情况，可能会重复出现。这时对点集S和点(i,j)的访问结果直接存储，可以省略重复访问后续节点的时间。
代码：
```
#include <utility>
vector<vector<int>> P;
int paths[20][20][1<<20];
const int pads[4][2] = {{0,1},{1,0},{0,-1},{-1,0}}, states = (1<<20)*sizeof(int);
int m, n;
int total_bits, path_bits;
class Solution {
public:
    int dfs(int i, int j, vector<vector<int>>& grid)
    {
        int ret = 0, bit = 1 << (i * n + j);

        if (grid[i][j] == 2) {
            if (path_bits == total_bits) {
                return 1;
            } else {
                return 0;
            }
        }

        if (paths[i][j][path_bits | bit] >= 0) {
            return paths[i][j][path_bits | bit];
        }

        P[i][j] = 1;
        path_bits |= bit;


        for (int k = 0; k < 4; k++) {
            int n_i = i + pads[k][0], n_j = j + pads[k][1];
            if (n_i >= 0 && n_i < m && n_j >= 0 && n_j < n &&
                grid[n_i][n_j] != -1 && !P[n_i][n_j]) {
                ret += dfs(n_i, n_j, grid);
            }
        }

        P[i][j] = 0;
        paths[i][j][path_bits] = ret;
        path_bits &= (~bit);

        return ret;
    }

    int uniquePathsIII(vector<vector<int>>& grid) {
        int s_i, s_j;

        path_bits = 0;
        total_bits = 0;
        m = grid.size(), n = grid[0].size();
        P = vector<vector<int>>(m, vector<int>(n, 0));

        for (int i = 0, bit = 1; i < m; i++) {
            for (int j = 0; j < n; j++, bit = bit << 1) {
                if (grid[i][j] < 0 || grid[i][j] == 2) {
                    continue;
                }
                if (grid[i][j] == 1) {
                    s_i = i;
                    s_j = j;
                }
                total_bits |= bit;
                memset(paths[i][j], -1, states);
            }
        }

        return dfs(s_i, s_j, grid);
    }
};
```

## 被围绕的区域 (lc 130)
solution: run Full DFS from all the 'O's on the edge rows or cols on the board, mark these 'O's visited, and for all non-visited elements, we replace them to 'X'.

complexity: time space O(mn)

code:
```
#include <utility>
using P = pair<int,int>;
class Solution {
    int m, n;
    vector<P> o_set, v_set;
    vector<vector<int>> parent, visited;
public:
    P find_next_point(P cur)
    {
        if (visited[cur.first][cur.second]){
            for (P &p : o_set) {
                cur = make_pair(-1,-1);
                if (!visited[p.first][p.second]) {
                    cur = p;
                    break;
                }
            }
        }
        return cur;
    }

    void dfs(vector<vector<char>>& board, int i, int j)
    {
        const int pads[4][2] = {{0,1},{1,0},{0,-1},{-1,0}};
        
        if (parent[i][j]) {
            return;
        }

        parent[i][j] = 1;

        for (int k = 0; k < 4; k++) {
            int n_i = i + pads[k][0], n_j = j + pads[k][1];
            if (n_i >= 0 && n_i < m && n_j >= 0 && n_j < n &&
                board[n_i][n_j] == 'O' && !visited[n_i][n_j]) {
                dfs(board, n_i, n_j);
            }
        }

        parent[i][j] = 0;
        visited[i][j] = 1;
    }

    void solve(vector<vector<char>>& board) {
        m = board.size(), n = board[0].size();
        P p;

        o_set.clear();
        v_set.clear();
        for (int i = 0; i < m; i++) {
            if (board[i][0] == 'O') {
                o_set.emplace_back(i,0);
            }
            if (board[i][n-1] == 'O') {
                o_set.emplace_back(i,n-1);
            }
        }
        for (int j = 1; j < n - 1; j++) {
            if (board[0][j] == 'O') {
                o_set.emplace_back(0,j);
            }
            if (board[m-1][j] == 'O') {
                o_set.emplace_back(m-1,j);
            }
        }

        visited = vector<vector<int>>(m, vector<int>(n,0));
        parent = vector<vector<int>>(m, vector<int>(n,0));

        if (o_set.size()) {
            p = o_set[0];
            while ((p = find_next_point(p)).first >= 0) {
                dfs(board, p.first, p.second);
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!visited[i][j]) {
                    board[i][j] = 'X';
                }
            }
        }
    }
};
```