# string

## 字符串中的查找与替换 (lc 833)
思路：此题难点主要是查找索引是乱序而非递增的，需要对查找索引排序。对比字符串时，直接对比target[i]和indice[i]处target[i]个长度的字符串是否相同即可（s.substr(indice[i], target[i].length())）。不要忘记先将上次索引与这次索引之间的字符先拼接入答案中。
复杂度：设索引长度为n，字符串长度为l。时间 O(nlogn + l)，空间 O(n + l)
代码：
```
class Solution {
public:
    string findReplaceString(string s, vector<int>& indices, vector<string>& sources, vector<string>& targets) {
        int n = indices.size();
        int last_i = 0, i;
        string result = "";
        vector<pair<int, int>> mp;

        for (int index = 0; index < n; index++) {
            mp.emplace_back(indices[index], index);
        }

        sort(mp.begin(), mp.end());

        for (int k = 0; k < n; k++) {
            i = mp[k].first;
            result += s.substr(last_i, i-last_i);
            if (i+sources[mp[k].second].length() <= s.length() && sources[mp[k].second] == s.substr(i, sources[mp[k].second].length())) {
                result += targets[mp[k].second];
                last_i = i + sources[mp[k].second].length();
            } else {
                last_i = i;
            }
        }

        if (last_i < s.length()) {
            result += s.substr(last_i, s.length()-last_i);
        }

        return result;
    }
};
```