# linklist

## 相交链表 (lc 160)
思路：从两个链表距离末尾相等的距离开始一起遍历（长度较小的链表从头部开始），如果遍历到某个节点是相同的，说明链表相交。（链表需要是无环的）
复杂度：时间 O(m + n)，空间 O(1)
代码：
```
class Solution {
public:
    int listlen(ListNode *node)
    {
        /* acyclic! */
        int len = 0;
        while (node) {
            node = node->next;
            len++;
        }
        return len;
    }

    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int m = listlen(headA), n = listlen(headB);
        ListNode *tailA;

        if (!m || !n) {
            return nullptr;
        }

        while (m > n) {
            headA = headA->next;
            m--;
        }

        while (n > m) {
            headB = headB->next;
            n--;
        }

        for (int i = 0; i < n; i++) {
            if (headA == headB) {
                return headA;
            } else {
                headA = headA->next;
                headB = headB->next;
            }
        }

        return nullptr;
    }
};
```

## 反转链表 (lc 206)
思路：遍历时，分祖先节点、访问节点、后继节点。将指向后继的访问节点，指向祖先，祖先变为该访问节点。继续访问下一个后继节点。当没有后继结点时，当前访问节点就是反转链表的头节点。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode *cur, *next, *prev;

        if (!head) {
            return nullptr;
        }

        next = head;
        prev = nullptr;
        while (next) {
            cur = next;
            next = next->next;
            cur->next = prev;
            prev = cur;
        }

        return cur;
    }
};
```

## 回文链表 (lc 234)
思路：实现O(1)的空间复杂度，需要利用递归算法：递归遍历链表，每个节点的完成顺序是链表的逆序。所以我们在每个递归函数栈中存一个链表顺序遍历的指针，用来和当前访问节点进行值的对比（是否相同）。当顺序遍历指针指向当前访问的节点或后继节点时，说明整个链表是回文的。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    int recursion(ListNode *back, ListNode **frontp)
    {
        ListNode *front;
        int ret;

        if (!back) {
            return 0;
        }

        if ((ret = recursion(back->next, frontp)) != 0) {
            return ret;
        }
        /* tricky! *frontp changed after calling recursion */
        front = *frontp;

        if (front == back || back->next == front) {
            return 1;
        }

        if (back->val != front->val) {
            return -1;
        }

        *frontp = front->next;

        return 0;
    }
    bool isPalindrome(ListNode* head) {
        ListNode *front = head;
        return (recursion(head, &front) > 0);
    }
};
```

## 合并 K 个升序链表 (lc 23)
思路：将k个链表头放入优先队列，每次取出最小值节点，加入合并链表，再将该节点的后继重新放入优先队列。空节点不入队。直至优先队列变为空队列。
复杂度：时间 O(nlogk)，空间 O(k)
代码：
```
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        priority_queue<pair<int, ListNode*>, vector<pair<int, ListNode*>>, greater<pair<int, ListNode*>>> heap;
        ListNode *head = new ListNode(), *cur = head;

        for (ListNode *node : lists) {
            if (node) {
                heap.push(make_pair(node->val, node));
            }
        }

        while (heap.size()) {
            auto p = heap.top();
            heap.pop();
            cur->next = p.second;
            cur = cur->next;
            if (cur->next) {
                heap.emplace(cur->next->val, cur->next);
            }
        }

        cur = head->next;
        head->next = nullptr;
        delete head;

        return cur;
    }
};
```