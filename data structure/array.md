# array

## 移动零 (lc 283)
思路：双指针i, j。i指向下一个需要填的位置，j指向下一个需要填的数。当a[j]为零时，跳过当轮循环。当i与j不相等时，令a[i] = a[j]。循环结束时，对i和j进行++。
终止条件：循环结束时，所有0到i-1的数组下标数字为非0。从i开始到n-1对数组填0。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int n = nums.size(), i = 0, j = 0;

        for (; j < n; j++) {
            if (!nums[j]) {
                continue;
            }
            if (i != j) {
                nums[i] = nums[j];
            }
            i++;
        }

        for (; i < n; i++) {
            nums[i] = 0;
        }
    }
};
```

## 盛最多水的容器 (lc 11)
思路：双指针。两条线之间的距离应尽可能长，所以从头尾开始寻找。每计算出一次盛水量后，缩短两条线之间的距离，这时应从短线开始移动，寻找更高的一条线，这样盛水量才可能更多。当两条线接触时，停止寻找。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    int maxArea(vector<int>& height) {
        int n = height.size();
        int i = 0, j = n - 1;
        int result = 0, tmp;

        while (i < j) {
            bool move_left = (height[i] < height[j]);
            tmp = move_left ? height[i] : height[j];
            result = max(result, (j - i) * tmp);
            if (move_left) {
                while (i < j && height[++i] <= tmp) ;
            } else {
                while (i < j && height[--j] <= tmp) ;
            }
        }

        return result;
    }
};
```

## 三数之和 (lc 15)
思路：先排序以便去重。外侧循环枚举第一个数，跳过重复的数。内侧循环与两数之和方法相同。但由于数组已经排序，我们使用双指针，当三数和大于target时，右指针左移，反之，左指针右移。
复杂度：时间 O(n^2)，空间 哈希 O(n) / 双指针 O(1)
代码：
```
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int n = nums.size(), tmp;
        vector<vector<int>> result;

        sort(nums.begin(), nums.end());

        for (int i = 0; i < n; i++) {
            if (i && nums[i] == nums[i - 1]) {
                continue;
            }

            int j = i + 1, k = n - 1;
            while (j < k) {
                if (nums[i] + nums[j] + nums[k] == 0) {
                    result.push_back(vector<int> {nums[i], nums[j], nums[k]});
                    do {
                        j++;
                    } while (j < k && nums[j] == nums[j - 1]);
                    do {
                        k--;
                    } while (j < k && nums[k] == nums[k + 1]);
                } else if (nums[i] + nums[j] + nums[k] > 0) {
                    do {
                        k--;
                    } while (j < k && nums[k] == nums[k + 1]);
                } else {
                    do {
                        j++;
                    } while (j < k && nums[j] == nums[j - 1]);
                }
            }
        }

        return result;
    }
};
```

## 任意子数组和的绝对值的最大值 (lc 1749)
思路：前缀和最大与最小之间的子数组，和的绝对值最大。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    int maxAbsoluteSum(vector<int>& nums) {
        int n = nums.size();
        int sum = 0, max_sum = 0, min_sum = 0;

        for (int i = 0; i < n; i++) {
            sum += nums[i];
            max_sum = max(max_sum, sum);
            min_sum = min(min_sum, sum);
        }

        return abs(max_sum - min_sum);
    }
};
```

## 最大子数组和 (lc 53)
思路：当前前缀和sum[i]与最小前缀和之间的差，即为数组nums[0:i]中最大子数组和。令i = 0 to n - 1。
复杂度：时间 O(n)，空间 O(1)
代码：
```
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        int sum = 0, max_sum = 0, min_sum = 0;

        for (int i = 0; i < n; i++) {
            sum += nums[i];
            max_sum = i ? max(max_sum, sum - min_sum) : sum;
            min_sum = min(sum, min_sum);
        }

        return max_sum;
    }
};
```

## 无重复字符的最长子串 (lc 3)
思路：双指针。当右指针指到重复元素时，左指针左移直到左指针滤过重复元素。使用哈希表记录所有子串中的元素。
代码：
```
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.length(), prefix_i = 0, result = 0;
        vector<int> hashtbl(256, 0);

        for (int i = 0; i < n; i++) {
            if (hashtbl[s[i]]) {
                result = max(result, i - prefix_i);
                while (prefix_i < i && (s[prefix_i] != s[i])) {
                    hashtbl[s[prefix_i]] = 0;
                    prefix_i++;
                }
                if (prefix_i < i) {
                    prefix_i++;
                }
            }
            hashtbl[s[i]] = 1;
        }
        result = max(result, n - prefix_i);

        return result;
    }
};
```

## 找到字符串中所有字母异位词 (lc 438)
思路：一个异位词字符数一定与原单词字符数一致，所以只需要找遍所有单词长度一致的子串，就能求解。
拓展：
1. 哈希表标识子串和原字符串的字母，使用vector时直接"=="号判断是否相同。
代码：
```
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        vector<int> dic(26, 0), hashtbl(26, 0), result;
        int n = s.length(), l = p.length();

        if (l > n) {
            return result;
        }

        for (int i = 0; i < l; i++) {
            hashtbl[s[i] - 'a']++;
            dic[p[i] - 'a']++;
        }

        if (hashtbl == dic) {
            result.emplace_back(0);
        }

        for (int i = 0; i < n - l; i++) {
            hashtbl[s[i] - 'a']--;
            hashtbl[s[i + l] - 'a']++;

            if (hashtbl == dic) {
                result.emplace_back(i + 1);
            }
        }

        return result;
    }
};
```

## 和为 K 的子数组 (560)
思路：用哈希表存储所有不同的前缀和各自的数量。和为：当前前缀和 - k 的数量即是和为k子数组的数量。
复杂度：时间 O(n)，空间 O(n)
拓展：
1. 使用unordered_map<int,int>存放，自定义的直接访问数组可能超出内存限制。
2. 每轮循环最后才更新哈希表。当前前缀和正好为k需要直接更新一次结果，因为当前前缀和减k为0的情况不包括空子数组。
代码：
```
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        int n = nums.size(), sum = 0, result = 0;
        unordered_map<int, int> prefix_hashtbl;

        for (int i = 0; i < n; i++) {
            sum += nums[i]; 
            if (sum == k) {
                result++;
            }
            if (prefix_hashtbl.find(sum - k) != prefix_hashtbl.end()) {
                result += prefix_hashtbl[sum - k];
            }
            prefix_hashtbl[sum]++;
        }

        return result;
    }
};
```

## 滑动窗口最大值 (lc 239)
思路：需要有一个数据结构存储滑动窗口内子数组的最大值——堆。但是堆不支持删除中间元素，所以我们需要统计每个元素是否有效，用一个哈希map记录所有元素出现的次数，当一个元素次数归零时，说明它不在滑动窗口内，此时当该元素出现在堆顶时，直接pop即可。
复杂度：时间 O(nlogn)，空间 O(n)
代码：
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        priority_queue<int> heap;
        unordered_map<int, int> hashmp;
        vector<int> result;
        int n = nums.size();

        for (int i = 0; i < k; i++) {
            heap.push(nums[i]);
            hashmp[nums[i]]++;
        }
        result.emplace_back(heap.top());

        for (int i = 0; i < n - k; i++) {
            if (nums[i] != nums[i + k]) {
                hashmp[nums[i]]--;
                while (heap.size() && hashmp[heap.top()] == 0) {
                    heap.pop();
                }
                hashmp[nums[i + k]]++;
                heap.push(nums[i + k]);
            }
            result.emplace_back(heap.top());
        }

        return result;
    }
};

## 最小覆盖子串 (lc 76)
思路：哈希表存所有字母出现次数。子串长度从0开始递增，每当加入一个字母到子串使得该字母出现次数多出所需时，统计一次是否覆盖，并且左移前缀起始点，找到一个最小覆盖。所有最小覆盖中，子串长度最短的即为所求。
复杂度：时间 O(C*m + n)，空间 O(C)，C为子串中所有不同字符的数量
拓展：
1. vector作为直接访问数组，用作哈希表。unordered_map也可以。
代码：
```
class Solution {
public:
    #define HASH(c) ((c <= 'Z') ? c - 'A' : c - 'a' + 26)

    inline bool valid(vector<int> &dic, vector<int> &hashtbl)
    {
        for (int i = 0; i < 52; i++) {
            if (hashtbl[i] < dic[i]) {
                return false;
            }
        }
        return true;
    }

    string minWindow(string s, string t) {
        vector<int> dic(52, 0), hashtbl(52, 0);
        int i = 0, j = 0, m = s.length(), n = t.length();
        int min_len = m + 1, m_i;

        if (m < n) {
            return "";
        }

        for (char c : t) {
            dic[HASH(c)]++;
        }

        for (j = 0; j < n; j++) {
            hashtbl[HASH(s[j])]++;
        }

        if (valid(dic, hashtbl)) {
            return s.substr(0, n);
        }

        for (j = n; j < m; j++) {
            int hash = HASH(s[j]);
            if (++hashtbl[hash] >= dic[hash]) {
                /* compute and shrink */
                if (valid(dic, hashtbl)) {
                    while (hashtbl[HASH(s[i])] > dic[HASH(s[i])]) {
                        hashtbl[HASH(s[i])]--;
                        i++;
                    }
                    if (min_len > j - i + 1) {
                        min_len = j - i + 1;
                        m_i = i;
                    }
                }
            }
        }

        return (min_len <= m) ? s.substr(m_i, min_len) : "";
    }
};
```

## 合并区间 (lc 56)
思路：
1. 方法一：哈希存储所有区间内的点，和点的后缀。每个点有后缀表示连通着下一个点。最后从所有区间最小和最大的点之间进行一次遍历，找出所有区间。
2. 方法二：对所有区间直接排序。默认对第一个数排序。再将所有相交的区间合并。
复杂度：
1. 方法一：时间 O(n + C)，空间 O(C)
2. 方法二：时间 O(nlogn)，空间 O(1)
代码：
```
#include <limits.h>

class Solution {
public:
/* hash
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        unordered_set<int> hashtbl, suffix;
        vector<vector<int>> result;
        int max_int = 1, min_int = INT_MAX;

        for (vector<int> &interv : intervals) {
            for (int num = interv[0]; num < interv[1]; num++) {
                hashtbl.insert(num);
                suffix.insert(num);
            }
            hashtbl.insert(interv[1]);
            min_int = min(min_int, interv[0]);
            max_int = max(max_int, interv[1]);
        }

        for (int num = min_int; num <= max_int; num++) {
            int start = num;
            while (num < max_int && suffix.count(num) && hashtbl.count(num + 1)) {
                num++;
            }
            result.emplace_back(vector<int> {start, num});
            while (num < max_int && !hashtbl.count(num + 1)) {
                num++;
            }
        }

        return result;
    }
*/
/* sort */
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        if (intervals.size() == 0) {
            return {};
        }
        sort(intervals.begin(), intervals.end());
        vector<vector<int>> merged;
        for (int i = 0; i < intervals.size(); ++i) {
            int L = intervals[i][0], R = intervals[i][1];
            if (!merged.size() || merged.back()[1] < L) {
                merged.push_back({L, R});
            }
            else {
                merged.back()[1] = max(merged.back()[1], R);
            }
        }
        return merged;
    }
};
```

## 轮转数组 (lc 189)
思路：环状swap。将位于x的元素移动到(x+k) mod n处，需要走gcd(k,n)圈。
复杂度：时间 O(n)，空间 O(1)
代码：
```
#include <algorithm>
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        k = k % n;
        int count = gcd(k, n);
        for (int start = 0; start < count; ++start) {
            int current = start;
            int prev = nums[start];
            do {
                int next = (current + k) % n;
                swap(nums[next], prev);
                current = next;
            } while (start != current);
        }
    }
};
```

## 在排序数组中查找元素的第一个和最后一个位置 (lc 34)
思路：二分查找target。
复杂度：时间 O(logn)，空间 O(1)
代码：
```
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        int n = nums.size();
        int low = 0, high = n - 1, mid = (low + high) >> 1, start = -1, end = -1;

        if (!n) {
            return vector<int>(2, -1);
        }

        if (nums[0] == target) {
            high = 0;
            start = 0;
        }

        while (low < high) {
            if (nums[high] == target) {
                mid = high;
            }
            if (nums[low] == target) {
                mid = low;
            }
            if (nums[mid] == target) {
                while (mid && nums[mid - 1] == target) {
                    mid--;
                }
                start = mid;
                break;
            } else if (nums[mid] > target) {
                high = mid;
            } else {
                low = mid;
            }
            if ((mid = (low + high) >> 1) == low) {
                break;
            }
        }

        if (start < 0) {
            return vector<int>(2, -1);
        }

        for (int i = start; i < n; i++) {
            if (i + 1 >= n || nums[i + 1] != target) {
                end = i;
                break;
            }
        }

        return vector<int> {start, end};
    }
};
```

## 数组中的第k大元素 (lc 215)
思路：快速排序法寻找下标为n-k的元素。每次选择一个元素，经过一次寻找后，确定该元素的下标i，比较i与n-k的大小，再从0到i-1或i+1到n-1选择一个元素重复寻找过程。
复杂度：时间 O(n)，空间 O(1)
拓展：
1. 第k小元素同样适用。
2. 选择元素不能简单选择第一个元素，使用随机数选择，时间性上更快。
代码：
```
class Solution {
public:
    void swap(int &a, int &b)
    {
        int tmp = a;
        a = b;
        b = tmp;
    }
    int findKthLargest(vector<int>& nums, int k) {
        int n = nums.size();
        int i = -1, x, high = n - 1, low = 0;

        srand(time(0));
        k = n - k;

        while (i != k) {
            int random = rand() % (high - low + 1) + low;
            swap(nums[low], nums[random]);
            i = low;
            x = nums[i];
            for (int j = i + 1; j <= high; j++) {
                if (nums[j] < x) {
                    i++;
                    if (i != j) {
                        swap(nums[i], nums[j]);
                    }
                }
            }
            swap(nums[low], nums[i]);

            if (i > k) {
                high = i - 1;
                low = 0;
            } else if (i < k) {
                low = i + 1;
            }
        }

        return nums[i];
    }
};
```