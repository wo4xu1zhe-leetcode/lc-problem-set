# hash

## 两数之和 (lc 1)
思路：建立一个哈希表，存放所有遍历数组得到的数，同时查找目标和与当前数之差是否已经在哈希表内。
复杂度：时间 O(n)，空间 O(n)
拓展：
1. 类似问题：三数之和、四数之和
2. 去重（三数之和）：对数组排序，最外层循环不重复访问。内层循环的两个数需要在一次遍历当中确定。
代码：
```
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_set<int> num_set;
        unordered_map<int, int> num_index;
        int n = nums.size();
        vector<int> result;

        for (int i = 0; i < n; i++) {
            if (num_set.find(target - nums[i]) != num_set.end()) {
                result.push_back(num_index[target - nums[i]]);
                result.push_back(i);
                break;
            } else {
                num_set.insert(nums[i]);
            }
            num_index[nums[i]] = i;
        }

        return result;
    }
};
```

## 字母异位词分组 (lc 49)
思路：以排序后的字符串作为哈希的key，key相同的字符串即为字母异位词。
复杂度：时间 O(n)，空间 O(n)
代码：
```
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> hashtbl;
        vector<vector<string>> result;

        for (string str :strs) {
            string sorted = str;
            sort(sorted.begin(), sorted.end());
            hashtbl[sorted].emplace_back(str);
        }

        for (auto pair : hashtbl) {
            result.push_back(pair.second);
        }

        return result;
    }
};
```

## 最长连续序列 (lc 128)
思路：使用哈希表记录每个数字。从小到大遍历一次哈希表，获取连续数字的长度。
复杂度：时间 O(n)，空间 O(n)
拓展：
1. 去重：只统计最长后缀，不考虑前缀。当某个数字在一个连续序列中间时，不考虑该数字。只有当某个数字前没有连续前缀时，才统计该数字的连续后缀的长度。
代码：
```
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> hashtbl;
        int result = 0, cnt = 0;

        for (int num : nums) {
            hashtbl.insert(num);
        }

        for (int num : hashtbl) {
            if (hashtbl.count(num - 1)) {
                continue;
            }
    
            cnt = 1;
            while (hashtbl.count(num + 1)) {
                cnt++;
                num++;
            }

            result = max(result, cnt);
        }

        return result;
    }
};
```