# tree

## 实现Trie前缀树 (lc 208)
思路：每个字符有26个子节点，分别代表该字符后续是否存在其他字符构成的单词。查找一次的只需要搜索一次前缀，将前缀输入遍历整棵树即可。
复杂度：
1. 时间：建立 O(1)，查找 O(|S|)，插入 O(|S|)
2. 空间：O(sigma(all string's length)|S|)
代码：
```
class Trie {
private:
    vector<Trie*> children;
    bool isEnd;

    Trie* searchPrefix(string prefix) {
        Trie* node = this;
        for (char ch : prefix) {
            ch -= 'a';
            if (node->children[ch] == nullptr) {
                return nullptr;
            }
            node = node->children[ch];
        }
        return node;
    }

public:
    Trie() : children(26), isEnd(false) {}

    void insert(string word) {
        Trie* node = this;
        for (char ch : word) {
            ch -= 'a';
            if (node->children[ch] == nullptr) {
                node->children[ch] = new Trie();
            }
            node = node->children[ch];
        }
        node->isEnd = true;
    }

    bool search(string word) {
        Trie* node = this->searchPrefix(word);
        return node != nullptr && node->isEnd;
    }

    bool startsWith(string prefix) {
        return this->searchPrefix(prefix) != nullptr;
    }
};
```

## 二叉树的中序遍历 (lc 94)
代码：
```
class Solution {
public:
    void traversal2(TreeNode* node, vector<int> &result)
    {
        if (!node) {
            return;
        }

        traversal2(node->left, result);
        result.emplace_back(node->val);
        traversal2(node->right, result);
    }
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> result;
        traversal2(root, result);
        return result;
    }
};
```

## 二叉树的最大深度 (lc 104)
思路：先序遍历。
代码：
```
class Solution {
public:
    void preTraversal(TreeNode *node, int h, int &depth)
    {
        if (!node) {
            return;
        }

        if (++h > depth) {
            depth = h;
        }

        preTraversal(node->left, h, depth);
        preTraversal(node->right, h, depth);
    }
    int maxDepth(TreeNode* root) {
        int depth = 0;
        preTraversal(root, 0, depth);
        return depth;
    }
};
```

## 从前序与中序遍历序列构造二叉树 (lc 105)
思路：递归，每次取preorder的下一个值，作为一个子树的根节点，然后在inorder中找出该子树包含的所有元素，当子树只有一个元素时说明到达叶子节点，创建完当前子树根节点便可以直接返回。
复杂度：时间 O(nlogn)，空间 O(n)
拓展：
1. 使用哈希映射所有inorder[i] -> i，可以将时间优化到O(n)
```
class Solution {
public:
    TreeNode * subtree(vector<int>& preorder, vector<int>& inorder, int &subroot_i, int low, int high)
    {
        TreeNode *node = new TreeNode(preorder[subroot_i]);

        if (low == high) {
            return node;
        }

        for (int mid = low; mid <= high; mid++) {
            if (inorder[mid] == preorder[subroot_i]) {
                if (mid == low) {
                    node->left = nullptr;
                } else {
                    subroot_i++;
                    node->left = subtree(preorder, inorder, subroot_i, low, mid - 1);
                }
                if (mid == high) {
                    node->right = nullptr;
                } else {
                    subroot_i++;
                    node->right = subtree(preorder, inorder, subroot_i, mid + 1, high);
                }
                break;
            }
        }

        return node;
    }
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        int subroot_i = 0;
        return subtree(preorder, inorder, subroot_i, 0, preorder.size() - 1);
    }
};
```

## 打家劫舍 III (lc 337)
solution: for a tree node, we can choose its two sons' val sum (if son is null, val = 0), or we can choose its val plus all its grand children's val.
A simple max(..., ...) will suggest the best result for us. Then we can simply traversal this tree, and compute each node's max_val, return the root's max_val as the answer.

code:
```
class Solution {
public:
    void traversal(TreeNode *node, int &dp_i_1, int &dp_i_2)
    {
        int dp_i_3, tmp1 = 0, tmp2 = 0;
        if (!node) {
            dp_i_1 = 0;
            dp_i_2 = 0;
            return;
        }

        traversal(node->left, tmp1, tmp2);
        dp_i_2 = tmp1;
        dp_i_3 = tmp2;
        traversal(node->right, tmp1, tmp2);
        dp_i_2 += tmp1;
        dp_i_3 += tmp2;

        dp_i_1 = max(dp_i_3 + node->val, dp_i_2);
    }

    int rob(TreeNode* root) {
        int dp_i_1, dp_i_2;
        traversal(root, dp_i_1, dp_i_2);
        return dp_i_1;
    }
};
```