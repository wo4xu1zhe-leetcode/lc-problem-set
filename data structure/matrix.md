# matrix

## 矩阵置零 (lc 73)
思路：选择矩阵的第0行和第0列分别作为列和行的0标记数组。第一次遍历矩阵，如果第i行第j列元素为0，就将matrix[0][j]和matrix[i][0]两个元素标记为0。第二次遍历矩阵，如果matrix[i][0]为0或matrix[0][j]为0，则matrix[i][j]也为0。开始遍历前，需要确定第0行和第0列是否有0元素，如果有，在两次遍历完矩阵后，需要将对应的第0行、第0列元素全部置0。
复杂度：时间 O(mn)，空间 O(1)
代码：
```
class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        int m = matrix.size();
        int n = matrix[0].size();
        int flag_col0 = false, flag_row0 = false;
        for (int i = 0; i < m; i++) {
            if (!matrix[i][0]) {
                flag_col0 = true;
            }
        }
        for (int j = 0; j < n; j++) {
            if (!matrix[0][j]) {
                flag_row0 = true;
            }
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (!matrix[i][j]) {
                    matrix[i][0] = matrix[0][j] = 0;
                }
            }
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (!matrix[i][0] || !matrix[0][j]) {
                    matrix[i][j] = 0;
                }
            }
        }
        if (flag_col0) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }
        if (flag_row0) {
            for (int j = 0; j < n; j++) {
                matrix[0][j] = 0;
            }
        }
    }
};
```

## 螺旋矩阵 (lc 54)
思路：分4个方向。每走完一个方向，更新一次i,j的上界或下界，保证ij一直走在没有走过的区域。初始时，i的上界为m，下届为-1，j的上界为n，下届为-1。走完i=0行后，令i的下界为0，后续类似。
复杂度：时间 O(mn)，空间 O(1)
代码：
```
class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        const int pads[4][2] = {{0,1},{1,0},{0,-1},{-1,0}};
        int m = matrix.size(), n = matrix[0].size();
        int i = 0, j = -1, k = 0;
        int low_i = -1, upp_i = m, low_j = -1, upp_j = n;
        vector<int> result;

        for (int cnt = 0; cnt < m * n;) {
            while ((i += pads[k][0]) > low_i && i < upp_i && (j += pads[k][1])> low_j && j < upp_j) {
                result.emplace_back(matrix[i][j]);
                cnt++;
            }
            switch (k) {
                case 0:
                    j = upp_j - 1;
                    low_i++;
                    break;
                case 1:
                    i = upp_i - 1;
                    upp_j--;
                    break;
                case 2:
                    j = low_j + 1;
                    upp_i--;
                    break;
                case 3:
                    i = low_i + 1;
                    low_j++;
                    break;
                default:
                    break;
            }
            k = (k + 1) % 4;
        }

        return result;
    }
};
```

## 旋转图像 (lc 48)
思路：由外向内一层一层旋转。
复杂度：时间 O(n^2)，空间 O(1)
代码：
```
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int n = matrix.size(), low = 0, high = n - 1;

        while (low < high) {
            for (int i = low; i < high; i++) {
                int tmp = matrix[low][i];
                matrix[low][i] = matrix[n - 1 - i][low];
                matrix[n - 1 - i][low] = matrix[high][n - 1 - i];
                matrix[high][n - 1 - i] = matrix[i][high];
                matrix[i][high] = tmp;
            }
            low++;
            high--;
        }
    }
};
```

## 搜索二维矩阵 II (lc 240)
思路：在每一行从右向左寻找，找到第一个小于target的数。在这个数的那一列向下寻找，找到第一个大于target数的行。重复这一过程直到找到target或某行没有任何数比target小，返回。
复杂度：时间 O(m + n)，空间 O(1)
代码：
```
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int m = matrix.size(), n = matrix[0].size();
        int r = 0, c = n - 1;

        if (matrix[0][0] == target) {
            return true;
        }

        for (int j = 1; j < n; j++) {
            if (target < matrix[0][j]) {
                c = j - 1;
                break;
            } else if (target == matrix[0][j]) {
                return true;
            }
        }

        for (; r < m && c >= 0; r++) {
            if (matrix[r][c] < target) {
                continue;
            }
            if (matrix[r][c] == target) {
                return true;
            }
            while (c >= 0 && matrix[r][c] > target) {
                c--;
            }
            if (c >= 0 && matrix[r][c] == target) {
                return true;
            }
        }

        return false;
    }
};
```